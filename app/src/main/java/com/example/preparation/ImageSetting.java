package com.example.preparation;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.example.preparation.adapter.ImageGetterAdapter;
import com.example.preparation.model.ImageGetterModel;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;

public class ImageSetting extends AppCompatActivity {

    private final static int REQUEST_CAMERA=100;
    private final static int RESULT_LOAD_IMAGE=101;

    private ArrayList<ImageGetterModel> imageGetterModelArrayList=new ArrayList<>();
    private ImageGetterAdapter mAdapter;
    private Button btnPick;
    private ImageView imageView;
    private RecyclerView recyclerView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_setting);


        recyclerView=findViewById(R.id.recycler_view);

        LinearLayoutManager layoutManager=new LinearLayoutManager(ImageSetting.this);
        layoutManager.setOrientation(layoutManager.HORIZONTAL);

        recyclerView.setLayoutManager(layoutManager);
        mAdapter=new ImageGetterAdapter(ImageSetting.this,imageGetterModelArrayList);

        recyclerView.setAdapter(mAdapter);

        imageView=findViewById(R.id.testing_image);
        btnPick=findViewById(R.id.btn_image);
        btnPick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogShowPhoto();
            }
        });


    }

    public void dialogShowPhoto() {
        String takephoto = "take Photo";
        String chooseFromLibrary = "gallery";
        String cancel = "cancel";
        String addPhoto = "add photo";
        final CharSequence[] items = {takephoto, chooseFromLibrary, cancel};
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setTitle(addPhoto);
        final String finalTakephoto = takephoto;
        final String finalChooseFromLibrary = chooseFromLibrary;
        final String finalCancel = cancel;
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (ActivityCompat.checkSelfPermission(ImageSetting.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
                    ActivityCompat.requestPermissions(ImageSetting.this,new String[] {Manifest.permission.READ_EXTERNAL_STORAGE},100);
                }
                if (items[item].equals(finalTakephoto)) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent, REQUEST_CAMERA);
                } else if (items[item].equals(finalChooseFromLibrary)) {
                    Intent intent = new Intent(
                            Intent.ACTION_PICK,
                            MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intent, RESULT_LOAD_IMAGE);
                } else if (items[item].equals(finalCancel)) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CAMERA && resultCode == Activity.RESULT_OK) {
            //selectedImageWork.setAlpha(1f);
            Bitmap photo = (Bitmap) data.getExtras().get("data");
            imageView.setImageBitmap(photo);
            Matrix mat = new Matrix();
            mat.postRotate(Integer.parseInt("270"));
            Bitmap bMapRotate = Bitmap.createBitmap(photo, 0, 0, photo.getWidth(), photo.getHeight(), mat, true);
            imageView.setImageBitmap(bMapRotate);
            imageGetterModelArrayList.add(new ImageGetterModel());
            mAdapter.updateList(imageGetterModelArrayList);

        }
        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && data != null) {
            Uri selectedImageURI = data.getData();
            ImageGetterModel placeWorkModel = new ImageGetterModel(); // the model between activity and adapter
            placeWorkModel.setImage(Integer.parseInt(convertImage2Base64()));  // here i pass the photo
            imageGetterModelArrayList.add(placeWorkModel);

            mAdapter.updateList(imageGetterModelArrayList); // add this

            mAdapter.notifyDataSetChanged();

        }
    }



    // this method will convert the image to base64
    public String convertImage2Base64() {
        Bitmap  bitmap = ((BitmapDrawable) imageView.getDrawable()).getBitmap();
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 90, stream);
        byte[] image = stream.toByteArray();
        return ("data:image/jpeg;base64," + Base64.encodeToString(image, 0));
    }
}
