package com.example.preparation.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.preparation.R;
import com.example.preparation.model.ImageGetterModel;

import java.util.ArrayList;
import java.util.List;

public class ImageGetterAdapter extends RecyclerView.Adapter<ImageGetterAdapter.Viewholder> {

    private Context context;
    private ArrayList<ImageGetterModel> imageGetterModelArrayList;

    public ImageGetterAdapter(Context context, ArrayList<ImageGetterModel> imageGetterModelArrayList) {
        this.context = context;
        this.imageGetterModelArrayList = imageGetterModelArrayList;
    }

    @NonNull
    @Override
    public ImageGetterAdapter.Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.image_item_layout,parent,false);

        return new Viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ImageGetterAdapter.Viewholder holder, int position) {
        ImageGetterModel listOfPictures = imageGetterModelArrayList.get(position);
        // loading picturesList using Glide library
        Glide.with(context).load(listOfPictures.getImage())
                .into(holder.imageView);
    }

    @Override
    public int getItemCount() {
        return imageGetterModelArrayList.size();
    }

    public class Viewholder extends RecyclerView.ViewHolder {

        private ImageView imageView;
        public Viewholder(@NonNull View itemView) {
            super(itemView);
            imageView=itemView.findViewById(R.id.image_view_getter);
        }
    }

    public void updateList(ArrayList<ImageGetterModel> imageGetterModelArrayList) {
        this.imageGetterModelArrayList = imageGetterModelArrayList;
        notifyDataSetChanged();
    }
}
