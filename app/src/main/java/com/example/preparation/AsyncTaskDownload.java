package com.example.preparation;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.InputStream;

public class AsyncTaskDownload extends AppCompatActivity {


    public String URL="";
    TextView editText;
    ImageView image;
    Button button;
    ProgressDialog mProgressDialog;
    private String downloadUrl = "http://www.9ori.com/store/media/images/8ab579a656.jpg";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_async_task_download);

        editText=findViewById(R.id.edittxt);

// Locate the ImageView in activity_main.xml
        image = (ImageView) findViewById(R.id.image);

// Locate the Button in activity_main.xml
        button = (Button) findViewById(R.id.button);

// Capture button click
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {

//get the image url from the text box
                editText.setText(downloadUrl);

// Execute DownloadImage AsyncTask
                new DownloadImage().execute(downloadUrl);
            }
        });
    }

    // DownloadImage AsyncTask
    private class DownloadImage extends AsyncTask<String, Void, Bitmap> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
// Create a progressdialog
            mProgressDialog = new ProgressDialog(AsyncTaskDownload.this);
// Set progressdialog title
            mProgressDialog.setTitle("Download Image Using AsyncTask Tutorial");
// Set progressdialog message
            mProgressDialog.setMessage("Downloading...please wait...");
            mProgressDialog.setIndeterminate(false);
// Show progressdialog
            mProgressDialog.show();
        }

        @Override
        protected Bitmap doInBackground(String... URL) {

            String imageURL = URL[0];

            Bitmap bitmap = null;
            try {
// Download Image from URL
                InputStream input = new java.net.URL(imageURL).openStream();
// Decode Bitmap
                bitmap = BitmapFactory.decodeStream(input);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap result) {
// Set the bitmap into ImageView
            image.setImageBitmap(result);
// Close progressdialog
            mProgressDialog.dismiss();
        }
    }
}