package com.example.preparation;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.MediaController;
import android.widget.VideoView;

public class MainActivity extends AppCompatActivity {

    private VideoView videoView;
    private Button btnClick;
    private MediaController mediaController;
    private final int VIDEO_RECIEVE=100;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        videoView=findViewById(R.id.videoview);
        btnClick=findViewById(R.id.btn_clk);
        //setMediaCont();
        this.mediaController=new MediaController(this);
        btnClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(Intent.createChooser(new Intent()
                        .setAction(Intent.ACTION_GET_CONTENT)
                        .setType("video/*"),"Select Video"),VIDEO_RECIEVE);
            }
        });

        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.setLooping(true);
            }
        });

    }

    private void playVideoRawFolder(){
        Uri uri=Uri.parse("android.resource://"+getPackageName()+"/");
    }

    private void setMediaCont(){
        mediaController.setMediaPlayer(videoView);
        mediaController.setAnchorView(videoView);
        videoView.setMediaController(mediaController);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode==VIDEO_RECIEVE &&resultCode==RESULT_OK){

            Uri videoUri=data.getData();

            if (videoUri !=null){
                videoView.setVideoURI(videoUri);
                videoView.start();
            }

        }
    }
}
